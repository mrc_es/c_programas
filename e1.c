#include<stdio.h>

int potenciar(unsigned int multiplicando, unsigned int potencia) {
	
	unsigned int potcont;
	
	potcont = 1;

	while (potencia != 0) {
		potcont *= multiplicando;
		--potencia;
	}

	return potcont;
}

void ask() {
	
	int numero, potencia;
	
	printf("\nIngrese numero a elevar: ");
	scanf("%d",&numero);

	printf("\nIngrese potencia: ");
	scanf("%d",&potencia);

	printf("\n%d\n",potenciar(numero, potencia));

}

int main() {

	ask();
	
	return 0;
}
