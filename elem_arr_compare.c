#include<stdio.h>
#include<stdlib.h>

#define NUM(a) (sizeof(a) / sizeof(int))

int arr_compare[30];

void llenar_array(int posicion, int elemento) {

    printf("posicion: %d, elemento: %d", posicion, elemento);    
    arr_compare[posicion] = elemento;

}

int comparar() {
    
    int maximo, elemento, i;
    
    for ( i = 1; i < (int)NUM(arr_compare); i++ ) {

        if (i == 1) {

            if (arr_compare[i] > arr_compare[i-1]) {
                maximo = arr_compare[i];
                printf("\ne: %d", maximo);           
            }
            else {
                maximo = arr_compare[i-1];
            }

        }

        if (arr_compare[i] > maximo)
            maximo = arr_compare[i];
    }

    return maximo;
}

void vista() {
    
    char *elemento;
    char *END = "salir";

    elemento = malloc(200);

    int posicion;

    posicion = 0;

    printf("Ingrese los elementos del array (escriba \"END\" para salir): \n");

    do {
        
        printf("ingrese elemento \"%d\": ", posicion);
        fgets(elemento,200,stdin);

        if ( *elemento != *END ) {
        
            llenar_array(posicion, atoi(elemento));
            posicion++;

        }

        printf("\n");

    } 
    while ( *elemento != *END );

    printf("\nEl numero maximo es: %d\n", comparar());

    free(elemento);

}

int main() {

    vista();

    return 0;
}
