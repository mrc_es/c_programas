#include<stdio.h>

int factorial(int x) {
    if (x > 1) {
        return x * factorial(x-1);
    }
    else {
        return 1;
    }
}

double euler(int x) {

    double t;

    if ( x > 0) {
        return 1.0/factorial(x) + euler(x-1);
    }
    else {
        return 1;
    }

}

int main() {

    int x = 20;

    printf("e es: %.4lf\n", euler(x));
    return 0;
}
