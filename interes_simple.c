#include<stdio.h>

float interes_simple(float prestamo, float tasa, int dias) {

    float interes;

    interes = (prestamo * tasa * dias)/365;

    return interes;
}

void preguntas() {

    float prestamo, tasa;
    int dias;

    do {
        printf("Introduzca el monto del prestamo (-1 para terminar): ");
        scanf("%f",&prestamo);

        if (prestamo != -1) {
            printf("Introduzca la tasa de interes: ");                                        
            scanf("%f",&tasa);
            printf("Introduzca el periodo del prestamo en días: ");
            scanf("%d",&dias);
            printf("El monto de interes es: $%.2f\n\n", interes_simple(prestamo, tasa, dias));
        }
    }
    while (prestamo != -1); 

}

int main() {

    preguntas();

    return 0;
}
