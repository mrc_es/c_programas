#include<stdio.h>

float pago_bruto(float horas, float pago_por_hora) {

    if ( horas > 40 ) {
    
       return pago_bruto(40, pago_por_hora) + pago_bruto( (horas - 40)*1.5, pago_por_hora);
    
    }
        
    return horas * pago_por_hora;
}

void preguntas() {

    float horas, pago_por_hora;

    do {
        printf("Introduzca el numero de horas laboradas (-1 para terminar): ");
        scanf("%f",&horas);

        if (horas != -1) {
            printf("Introduzca el pago por hora del empleado: ");                                        
            scanf("%f",&pago_por_hora);
            printf("El salario es de: $%.2f\n\n", pago_bruto(horas, pago_por_hora));
        }
    }
    while (horas != -1); 

}

int main() {

    preguntas();

    return 0;
}
