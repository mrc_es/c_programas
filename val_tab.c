#include<stdio.h>
#define NUM(a) (sizeof(a) / sizeof(int))

void vista() {

    int numeros[10] = {1,2,3,4,5,6,7,8,9,10};
    int indice;

    printf("N\t10*N\t100*N\t1000*N");

    for (indice=0; indice < NUM(numeros); indice++) {
        printf("\n%d\t%d\t%d\t%d",numeros[indice],numeros[indice]*10,numeros[indice]*100,numeros[indice]*1000);
    }
    printf("\n");

}

int main() {
    vista();
    return 0;
}
